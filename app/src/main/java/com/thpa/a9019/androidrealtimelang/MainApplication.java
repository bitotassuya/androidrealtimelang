package com.thpa.a9019.androidrealtimelang;

import android.app.Application;
import android.content.Context;

import com.thpa.a9019.androidrealtimelang.Helper.LocaleHelper;

import java.util.Locale;

public class MainApplication extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base,"en"));
    }
}
